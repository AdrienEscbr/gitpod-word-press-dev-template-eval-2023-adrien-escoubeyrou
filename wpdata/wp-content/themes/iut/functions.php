<?php

function iut_wp_enqueue_scripts() {
	$parenthandle = 'twentynineteen-style';
	$theme        = wp_get_theme();

	wp_enqueue_style( 
    	$parenthandle,
		get_template_directory_uri() . '/style.css', 
		array(),
		$theme->parent()->get( 'Version' )
	);


	wp_enqueue_style( 
    	'iut-style',
		get_stylesheet_uri(), 
		array( $parenthandle ),
		$theme->get( 'Version' )
	);
}

add_action( 'wp_enqueue_scripts', 'iut_wp_enqueue_scripts' );



function iut_register_post_type_recipe()
{
  register_post_type(
    'recipe',
    array(
      'labels' => array(
        'name' => __('Recipe'),
        'singular_name' => __('Recipe'),
      ),
      'public' => true,
      'publicly_queryable' => true,
      'menu_icon' => 'dashicons-carrot',
      'has_archive' => 'recettes',
      'show_in_rest' => true,
      'herarchical' => false,
      'supports' => array('title', 'editor'),
      'rewrite' => array('slug' => 'recette'),
    )
  );
}

add_action('init', 'iut_register_post_type_recipe', 10);








function iut_add_meta_boxes_recipe($post) {
	add_meta_box(
		'iut_mbox_recipe',                 	// Unique ID
		'Infos complémentaires',      // Box title
		'iut_mbox_recipe_content',  			// Content callback, must be of type callable
		'recipe'                            	// Post type
	);
}

add_action('add_meta_boxes', 'iut_add_meta_boxes_recipe');

function iut_mbox_recipe_content($post) {
	// Get meta value
	$iut_ingredients = get_post_meta(
		$post->ID,
		'iut-ingredients',
		true
	);

	

	echo '
	<p>
		<label for="iut-ingredients">Ingredients : </label>
        
        <textarea id="iut-ingredients" name="iut-ingredients">' . $iut_ingredients .'</textarea>

			
	</p>
	';

}

// Save post meta
function iut_save_post($post_id) {
	if (isset($_POST['iut-ingredients'])) {
		update_post_meta(
			$post_id,
			'iut-ingredients',
			sanitize_text_field($_POST['iut-ingredients'])
		);
	}
}

add_action('save_post', 'iut_save_post');